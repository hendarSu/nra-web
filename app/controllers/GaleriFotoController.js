'use strict';

const Promise = require('bluebird');

module.exports = {
    index: Async.route(function *(req, res, next) {
        var model = res.model;
        res.render('content/galeri_photo', model);
    }),

};