'use strict';
const passport = require('passport');
const Promise = require('bluebird');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

function wrap(genFunction) {
    const cr = Promise.coroutine(genFunction);

    return function(username, password, done) {
        cr(username, password, done).catch(done);
    };
}

function getEmployee(email) {
    return Employee
        .where({ email: email })
        .fetch()
        .catch(console.error);
}

const handleAuth = wrap(function *(email, password, done) {
    const employee = yield getEmployee(email);
    if (_.isEmpty(employee)) {
        return done(null, false, { type: 'email', message: 'The email you entered does not belong to any account.' });
    }

    if (!bcrypt.compareSync(password, employee.toJSON().password)) {
        return done(null, false, { type: 'password', message: 'The password you entered is incorrect. Please enter again.', email: email });
    }

    return done(null, employee.toJSON());
});

const strategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
}, handleAuth);

passport.use(strategy);

