
const express = require('express');
const router = express.Router();

var auth = require('../middlewares/authenticated');
var multipart = require('../middlewares/multipart');

// Index
// router.get('/', AuthenticationController.index);
// router.get('/dashboard', DashboardController.index);

// Auth
// router.post('/auth/login', AuthenticationController.login);
// router.get('/auth/logout', AuthenticationController.logout);
// router.get('/forgot/:email?', AuthenticationController.forgot);
// router.post('/auth/forgot', AuthenticationController.forgotProcess);

//
// router.get('/employees', /*auth.isAuthenticated(),*/ EmployeeController.index);
// router.get('/employees/get', auth.isAuthenticated(), EmployeeController.get);
// router.post('/employees/add', /*auth.isAuthenticated(),*/ EmployeeController.add);


//
router.get('/', IndexController.index);
router.get('/tentang_kami', TentangKamiController.index);
router.get('/manajemen', DariManajemenController.index);
router.get('/perjalanan_haji', PerjalananHajiController.index);
router.get('/perjalanan_umrah', PerjalananUmrahController.index);
router.get('/promo-brosur', PromoController.index);
router.get('/nratv/tv-streaming', NraTvController.index);
router.get('/index_video', IndexVideoController.index);
router.get('/berita/index-berita', IndexBeritaController.index);
router.get('/berita/baca', BeritaController.index);
router.get('/galeri_photo', GaleriFotoController.index);
router.get('/testimonial', TestimonialController.index);
router.get('/hubungikami', HubungiKamiController.index);
router.get('/nratv/galeri-video', VideoController.index);


module.exports = router;

